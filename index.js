/*
import "babel-polyfill"
import Fade from '.src/Fade.jsx';
export {FadeBlocks, Animation} from '.src/Fade.jsx';
export default Fade
*/

var Fade = require('./dist/Fade.js')
module.exports = Fade

module.exports.ScrollTrigger = Fade.ScrollTrigger
module.exports.Animator = Fade.Animator
