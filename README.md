react-move-stuff
--------------------------
A quick and easy way to add animations, including optimized scroll triggers.

---------------------------------
### Components
---------------------------------

`<Fade />`
 - Fades in with animation customized by props (see examples below) -- triggered automatically when scrolled to

<br />

`<Animator />`
- Lower level version of `<Fade>` which is triggered by the `triggered` prop instead and doesn't automatically animate opacity (uses `fade` prop)

<br />

`<ScrollTrigger />`
- component fires `props.onTrigger` and `props.onReset` depending on the scroll position and the value of `props.repeat`. Implemented by `<Fade>` under the hood.

<br />


---------------------------------
### Usage (es6)
---------------------------------
```
import Fade, {Animator, ScrollTrigger} from 'react-move-stuff';

...

  // Fade Component

  <Fade> Fades in when scrolled to </Fade>

  <Fade up right> Same as above but moves up and right </Fade>

  <Fade zoom-out angle={45}> zoom effect while moving in from 45° </Fade>

  <Fade spin-right> spins 180° while fading in </Fade>

  <Fade up duration={5} delay={5}> will fade up for 5s, after 5s delay </Fade>

  <Fade up down left right> Fades without moving since up cancels down, etc. </Fade>




  <ScrollTrigger onTrigger={ console.log('Triggered') }>
    will log "Triggered" to the console when scrolled to
  </Scroll>



  <Animator fade triggered={true}> toggling `triggered` fades in/out </Animator>

...

```


`TODO:` full documentation
